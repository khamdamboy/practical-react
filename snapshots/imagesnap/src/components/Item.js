
import React from "react";
import Container from "./Container";
const Item = ({searchTerm}) => {
    return(
        <div>
            <h2>
                {searchTerm} Pictures
                <Container searchTerm = {searchTerm}/>
            </h2>
        </div>
    );
};

export default Item;