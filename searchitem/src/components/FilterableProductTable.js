import React, {Component} from 'react';
import ProductTable from './ProductTable';
import SearchBar from './SearchBar';

class FilterableProductTable extends Component {

    // 4단계에서 추가
    constructor(props) {
        super(props);
        this.state = {
            filterText: '',
            inStockOnly: false
        }

        // 5단계에서 이벤트 함수 추가
        this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
        this.handleInStockChange = this.handleInStockChange.bind(this);
    }

    // 5단계에서 이벤트 함수 추가 : 검색어로 state 변경
    handleFilterTextChange(filterText) {
        this.setState ({
            filterText : filterText,
        });
    }

    // 5단계에서 이벤트 함수 추가 : 재고 확인 변경
    handleInStockChange(inStockOnly) {
        this.setState ({
            inStockOnly : inStockOnly
        });
    }

    render() {

        return (
            <div>
                {/* 4단계에서 filterText, inStockOnly props 추가 */}
                <SearchBar
                    filterText={this.state.filterText}
                    inStockOnly={this.state.inStockOnly}
                    onFilterTextChange={this.handleFilterTextChange} // 5단계에서 이벤트 함수를 props로 넘김
                    onInStockChange={this.handleInStockChange}  // 5단계에서 이벤트 함수를 props로 넘김
                />
                {/* 4단계에서 filterText, inStockOnly props 추가 */}
                <ProductTable
                    products={this.props.products}
                    filterText={this.state.filterText}
                    inStockOnly={this.state.inStockOnly}
                />
            </div>
        );
    }
}

export default FilterableProductTable;
